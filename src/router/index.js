import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Rank from '../views/Rank.vue'
import Podcast from '../views/Podcast.vue'
import Songs from '../views/Songs.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/rank',
    name: 'Rank',
    component: Rank
  },
  {
    path: '/songs',
    name: 'Songs',
    component: Songs
  },
  {
    path: '/podcast',
    name: 'Podcast',
    component: Podcast
  }
]

const router = new VueRouter({
  routes
})

export default router
