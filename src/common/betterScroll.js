import BScroll from "better-scroll"; //引入betterscroll

/**
 * 函数表达式，横向滚动的方法
 * @param {*} that 代表当前实例（vue）
 * @param {*} wrapper  代表滚动的容器
 * @param {*} content 代表滚动的内容区
 * @param {*} num 代表滚动项的数目
 * @param {*} itemWidth 代表滚动项的宽度（单位是rem）
 */
let betterScrollHorizontal = function (that, wrapper, content, num, itemWidth) {
    //动态获取到内容区content的宽度
    content.style.width = num * itemWidth + "rem";
    that.$nextTick(() => {
        //wrapper为template中容器的ref，表示给那个元素来设置滚动
        that.scroll = new BScroll(wrapper, {
            scrollY: false, //关闭纵向滚动
            scrollX: true, //横向滚动
            startX: 0, //横向滚动的开始位置
            click: true //开启浏览器的click事件
        });
    });
}

export { betterScrollHorizontal };