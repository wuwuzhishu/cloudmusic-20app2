import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import request from './common/request.js'
import { Lazyload } from 'vant';
Vue.use(Lazyload);

Vue.use(Vant);

Vue.config.productionTip = false
Vue.prototype.$request = request.httpRequest

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
