module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://171.220.242.237:3001',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''  //通过pathRewrite重写地址，将前缀/api转为/
                }
            }
        }
    },
}
